# PHP Orientado a Objetos

En este repositorio se incluyen las actividades para la unidad **PHP Orientado a Objetos** del programa de capacitación en Ingeniería de Software de la DGTIC.
- **Ejercicios de HTML**  
Captura de pantalla en la carpeta *Ejercicio_HTML*.
- **Ejercicios de SQL**  
Captura de pantalla en la carpeta *Ejercicio_SQL*.
- **Ejercicios de PHP**  
Captura de pantalla en la carpeta *Ejercicio_PHP*.
- **Ejercicio 1: Clases y objetos**  
La actividad se encuentra en los archivos dentro del directorio */Clases/ejercicio1* y en el archivo */Vistas/vistaEjercicio1.php*.
- **Ejercicio 2: Metodos y atributos**  
La actividad se encuentra en los archivos */Clases/Carro2.php* y */Vistas/vistaEjercicio2.php*.
- **Ejercicio 3: Constructores y destructores**  
La actividad se encuentra en los archivos dentro del directorio */Clases/ejercicio5* y en el archivo */Vistas/vistaEjercicio5.php*.
- **Ejercicio 4: Herencia**  
La actividad se encuentra en los archivos dentro del directorio */Clases/ejercicio6* y en el archivo */Vistas/vistaEjercicio6.php*.

## Autor
- José Yair Castro Rojas

## Asesores
- Daniel Barajas González
- Hugo German Cuellar Martínez
- Karla Fonseca