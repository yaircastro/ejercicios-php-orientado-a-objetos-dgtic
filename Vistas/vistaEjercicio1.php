<?php

class Carro{
	//declaracion de propiedades
	public $color;
	
}

//declaración de la clase "Moto"
class Moto{
	//declaración de las propiedades de la clase "Moto"
	public $peso;
	public $tipo;
}


//inicialización de los mensajes que lanzara el servidor
$mensajeServidor='';
$mensajeServidor1='';
$mensajeServidor2='';

//creación de la instancia "Moto1" a partir de la clase "Moto"
$Moto1 = new Moto;
//creación de la instancia "Carro1" a partir de la clase "Carro"
$Carro1 = new Carro;

//condición para desplegar el mensaje en la página si la solicitud POST no se encuentra vacía
 if (!empty($_POST)){
 	//almacenamos el valor mandado por POST en el atributo color
 	$Carro1->color=$_POST['color'];
 	//se construye el mensaje que sera lanzado por el servidor
 	$mensajeServidor='el servidor dice que ya escogiste un color: '.$_POST['color'];
	
	//almacenamiento del valor enviado por POST y construcción del mensaje lanzado por el servidor
	$Moto1->peso=$_POST['peso'];
	$mensajeServidor1='el servidor dice que ya especificaste el peso de la moto: '.$_POST['peso'];
	
	//almacenamiento del valor enviado por POST y construcción del mensaje lanzado por el servidor
	$Moto1->tipo=$_POST['tipo'];
	$mensajeServidor2='el servidor dice que ya especificaste el tipo de moto: '.$_POST['tipo'];
 }

?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>

	<input type="text" class="form-control" value="<?php  echo $mensajeServidor1; ?>" readonly>

	<input type="text" class="form-control" value="<?php  echo $mensajeServidor2; ?>" readonly>
	
	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">

			 <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
			<div class="col-sm-4">
			</div>
			
			<label class="col-sm-3" for "CajaTexto2">Peso de la moto (Kg):</label>
			<div class="col-sm-4">
				<input class="form-control" type="number" step="0.1" name="peso" id="CajaTexto2">
			</div>
			<div class="col-sm-4">
			</div>
			
			<label class="col-sm-3" for "CajaTexto3">Tipo de moto:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="tipo" id="CajaTexto3" placeholder="Scooter, deportiva, chopper, etc.">
			</div>
			
		</div>
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>


</body>
</html>

