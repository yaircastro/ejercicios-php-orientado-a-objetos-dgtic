<?php

// La clase debe hacer lo mismo que la clase Token, pero devolviendo una contraseña de 4 letras mayúsculas
	class clave{
		private $nombre;
		private $pass;
		
		//declaración de la función randomString
		function randomString() {
			
		}
		
		// método constructor
		public function __construct($nombre_front) {
			$this->nombre=$nombre_front;
			
		// definiendo el cuerpo de la función randomString() para generar una cadena aleatoria de 4 letras mayúsculas
			function randomString() {
				$longitud = 4;
				$caracteres = 'ABCDEFGHIJKLMNROPQRSTUVWXYZ';
				$random = substr(str_shuffle($caracteres), 0, $longitud);
				return $random;
			}
		//asignación de un valor aleatorio a la propiedad "pass" con la función randomString()
		$this->pass=randomString();
		}
		
		// método mostrar para el mensaje que desplegará la página
		public function mostrarClave(){
			return 'Hola '.$this->nombre.' esta es tu contraseña: '.$this->pass;
		}
		
		//metodo destructor
		public function __destruct(){
			$this->clave='La contraseña ha sido destruida';
			echo $this->clave;
		}
	}
	
	$mensaje = '';
	
	if(!empty($_POST)) {
	//creacion del objeto derivado de la clase "clave"
	$clave1= new clave($_POST['nombre']);
	//asignación del valor aleatorio a la propiedad "pass" del objeto
	$pass=$clave1->randomString();
	//mensaje que muestra la clave generada y asignada a la propiedad "pass" de este objeto
	$mensaje=$clave1->mostrarClave();
	}
?>