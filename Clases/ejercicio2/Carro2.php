<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $fabricacion;
	private $circula;
	//declaracion del método verificación con condiciones if/elseif/else para devolver un resultado según el año de fabricación
	public function verificacion($fabricacion){
		if ($fabricacion < 1990) {
			$this->circula = 'No';
		} elseif ($fabricacion >= 1990 and $fabricacion <= 2010) {
			$this->circula = 'Revisión';
		} else {
			$this->circula = 'Sí';
		}
	}
	
	// Se utiliza el método __toString() para poder desplegar la propiedad privada "circula"
	public function __toString(){
		return $this->circula;
	}
}


//creación de instancia a la clase Carro
$Carro1 = new Carro2();

// Obtención de los datos pasados por el método POST para integrarlos como valores de las propiedades del objeto
if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->fabricacion=$_POST['fabricacion'];
}

//invocación del método "verificación" para que el objeto "Carro1" obtenga el valor que le corresponda a su propiedad "circula"
$Carro1 -> verificacion($Carro1->fabricacion);
?>