<?php

	include_once('transporte.php');

//esta es la nueva clase que se pide en las instrucciones del Ejercicio 4	
	class espacial extends transporte{
		private $partes;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$par){
			parent::__construct($nom,$vel,$com);
			$this->partes=$par;
		}

		// sobreescritura de metodo
		public function resumenEspacial(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Número de partes:</td>
						<td>'. $this->partes.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';

?>